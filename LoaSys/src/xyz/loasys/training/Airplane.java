package xyz.loasys.training;

public class Airplane {
	
	public String name;
    public String modell;
    public int maxSpeed;

    Airplane() {
    	
        name = "Flugzeug";
        modell = "Messerschmitt Me 262";
        maxSpeed = 530;
        
    }

    Airplane(String name, String modell, int maxSpeed) {
    	
        this.name = name;
        this.modell = modell;
        this.maxSpeed = maxSpeed;
        
    }

    public void fly(int speed) {
    	
        if(speed <= maxSpeed) {
        	
            System.out.println("Das Flugzeug " + name + " des Modells " + modell + " fliegt " + speed + "mph");
        
        } else {
        	
            System.out.println("Das Flugzeug " + name + " des Modells " + modell + " darf nur maximal " + maxSpeed + "mph fliegen!");
        
        }
    }

    public static void flyAll(Airplane[] names, int speed) {
    	
        for(int i = 0; i <= names.length-1; i++) {
        	
            names[i].fly(speed);
            
        }
    }

    public static void test(int speed) {
    	
        Airplane f01 = new Airplane("Flugzeug01","Modell01",530);
        Airplane f02 = new Airplane("Flugzeug02","Modell02",330);
        Airplane f03 = new Airplane("Flugzeug03","Modell03",130);
        Airplane f04 = new Airplane("Flugzeug04","Modell04",230);

        Airplane[] a = {f01,f02,f03,f04};
        flyAll(a,speed);
        
    }

}
