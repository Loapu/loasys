package xyz.loasys.training;

public class Animal {
	
	private String name;
    private String species;
    private String sound;
    private int age;

    Animal() {
    	
        name = "Bob";
        species = "Kuh";
        sound = "muh";
        age = 5;
        
    }

    Animal(String name, String species, String sound, int age) {
    	
        this.name = name;
        this.species = species;
        this.sound = sound;
        this.age = age;
        
    }

    public int getAge() {
    	
        return this.age;
        
    }

    public String makeSound(int quantity) {
    	
        String sound = "";
        
        for(int i = 0; i <= quantity; i++) {
        	
            sound += this.sound;
            
        }
        
        return sound;
        
    }

    public String getIdentity() {
    	
        return "Mein Name ist " + this.name + " und ich bin ein/eine " + this.species;
        
    }

    public static String countAllAges(Animal[] names) {
    	
        int age = 0;
        
        for(int i = 0; i <= names.length-1; i++) {
        	
            age += names[i].getAge();
            
        }
        
        return "Zusammen sind wir " + age + " Jahre alt.";
        
    }

    public static String test() {
    	
        Animal i1 = new Animal();
        Animal i2 = new Animal();
        Animal i3 = new Animal();
        Animal i4 = new Animal();

        Animal[] a = {i1,i2,i3,i4};

        return countAllAges(a);
        
    }

}
