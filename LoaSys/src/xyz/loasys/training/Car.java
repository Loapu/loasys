package xyz.loasys.training;

public class Car {
	
	protected float tacho = 0;
	
    public void setZero() {
    	
        tacho = 0;
        
    }
    
    public void drive(float n, float m) {
    	
        tacho += n*m;
        
    }
    
    static float mileage(float n, float m) {
    	
        return n/100/m;
        
    }

}
