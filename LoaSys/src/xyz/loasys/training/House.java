package xyz.loasys.training;

public class House {
	
	public static String[] energieKlasse = {"A","B","C"};
	
    public static float schaetzeKubikmeter(float quadratmeter) {
    	
        return quadratmeter * 2.3f;
        
    }

    private float grundflaeche;
    private int stockwerke = 1;
    private boolean hatKeller = false;

    House(float grundflaeche) {
    	
        this.grundflaeche = grundflaeche;
        
    }

    public void setzeKeller(boolean ja_nein) {
    	
        hatKeller = ja_nein;
        
    }

    public float berechneGesamtflaeche() {
    	
        float flaeche = grundflaeche * stockwerke;
        
        if (hatKeller) {
        	flaeche += grundflaeche;
        }
        
        return flaeche;
        
    }
}
