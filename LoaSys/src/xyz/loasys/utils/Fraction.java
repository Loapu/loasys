package xyz.loasys.utils;

public class Fraction {

	public String bruch;
	public long numerator;
	public long denominator;

	Fraction(long numerator) {
		
		this.numerator = numerator;
		this.denominator = 1;
		this.bruch = numerator + "/" + 1;
		
	}

	Fraction(long numerator, long denominator) {
		
		this.numerator = numerator;
		this.denominator = denominator;
		this.bruch = numerator + "/" + denominator;
		
	}

	public Fraction inv() {
		
		if (numerator == 0) {
			
			return this;
			
		}
		
		return new Fraction(denominator, numerator);
		
	}

	public static int gcd (int n, int m) {
		
		int a = 0;
		
		n = Math.abs(n);
		m = Math.abs(m);
		
		for (int i = 1; i <= m; i++) {
			
			if (n%i == 0 && m%i == 0) {
				
				a = i;
				continue;
				
			} 
		}
		
		return a;
	}

}
