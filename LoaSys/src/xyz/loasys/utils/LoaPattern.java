package xyz.loasys.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class LoaPattern {

	private static Date timestamp = new Date();
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy,MM,dd");
	private static String heute = simpleDateFormat.format(timestamp);

	public static void isTime() {
		System.out.println("Bitte benutze das Format YYYY,MM,DD");
		System.out.println("Beispiel: " + heute);
		System.out.println("------------------------------ WARNUNG ------------------------------");
		System.out.println("Sollte eine Null (0) vor dem Monat oder dem Tag stehen (z.B. 09) gilt es die Null zu vernachlaessigen.\nBei nicht-beachtung dieses Hinweises ist es durchaus moeglich, dass es zu Fehlern im System kommen kann.");
		System.out.println("------------------------------ WARNUNG ------------------------------");
	}

	public static void isTime(int a, int b, int c) {

		String aS = Integer.toString(a);
		String bS = Integer.toString(b);
		String cS = Integer.toString(c);

		if(aS.length() == 0) aS = "0000";
		if(aS.length() == 1) aS = "000" + aS;
		if(aS.length() == 2) aS = "00" + aS;
		if(aS.length() == 3) aS = "0" + aS;

		if(bS.length() == 0) bS = "00";
		if(bS.length() == 1) bS = "0" + bS;

		if(cS.length() == 0) cS = "00";
		if(cS.length() == 1) cS = "0" + cS;

		String eingabe = aS + "-" + bS + "-" + cS;
		String eingabeDF = aS + "," + bS + "," + cS;
		boolean heute_bl = heute.equals(eingabeDF);

		if(!Pattern.matches("\\d\\d\\d\\d-\\d\\d-\\d\\d",eingabe)) { 
			isTime();
		} else {

			System.out.println("easyDate: " + Pattern.matches("\\d\\d\\d\\d-(0[1-9]|1[0-2])-(0[1-9]|[1-2]\\d|3[0-1])",eingabe));
			System.out.println("realDate: " + Pattern.matches("\\d\\d\\d\\d-(((0[13578]|1[02])-(0[1-9]|[1-2]\\d|3[0-1]))|(02-(0[1-9]|1[0-9]|2[0-8]))|((0[469]|11)-(0[1-9]|[1-2]\\d|30)))",eingabe));
			System.out.println("today: " + heute_bl);
		}
	}

	public static void datum() {
		System.out.println(heute);
	}

}
