package xyz.loasys.utils;

import java.util.Random;

public class Security {
	
	public static String operate(String md5) {
		
		Random r = new Random();
		int count = r.nextInt(20000);
		md5 += count;
		
        try {
        	
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            
            byte[] array = md.digest(md5.getBytes());
            
            StringBuffer sb = new StringBuffer();

            for (int i = 0; i < array.length; ++i) {
            	
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
                
            }
            return sb.toString();

        } catch (java.security.NoSuchAlgorithmException e) {
        }
        
        return null;
    }
	
	public static String operate() {
		return Security.operate("");
	}

    public static String allowedFor(int age) {

        int group;
        
        if(age < 6) {
        	
            group = 0;
            
        } else if(age < 12) {
        	
            group = 1;
            
        } else if(age < 16) {
        	
            group = 2;
            
        } else if(age < 18) {
        	
            group = 3;
            
        } else {
        	
            group = 4;
            
        }

        String[] ageArray = {"FSK 0","FSK 6","FSK 12","FSK 16","FSK 18"};

        return ageArray[group];
    }

}
